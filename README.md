# TortoiseGit 安装包及语言环境下载

本仓库提供 TortoiseGit 的安装包以及适用于32位和64位系统的语言环境文件。TortoiseGit 是一个基于 Git 版本控制系统的图形化客户端，方便用户在 Windows 系统上进行版本管理操作。

## 资源内容

- **TortoiseGit 安装包**：包含 TortoiseGit 的最新版本安装文件，支持 Windows 系统。
- **32位语言环境**：适用于32位系统的 TortoiseGit 语言包。
- **64位语言环境**：适用于64位系统的 TortoiseGit 语言包。

## 使用说明

1. **下载安装包**：选择适合您系统的 TortoiseGit 安装包进行下载。
2. **安装 TortoiseGit**：运行下载的安装包，按照提示完成 TortoiseGit 的安装。
3. **安装语言环境**：根据您的系统位数，下载并安装相应的语言环境文件，以获得更好的使用体验。

## 注意事项

- 请确保您的系统满足 TortoiseGit 的最低系统要求。
- 安装语言环境前，请先完成 TortoiseGit 的安装。

## 其他说明

本仓库将持续更新，确保提供最新版本的 TortoiseGit 安装包及语言环境文件。如有任何问题或建议，欢迎反馈。

啦啦啦啦~